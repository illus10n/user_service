#!/bin/bash
CURRENT_DIR=$GOPATH/src/bitbucket.org/illus10n/blog_protos/post_service/
PATH="${PATH}:${HOME}/code/go/bin"
for x in $(find ${CURRENT_DIR}/* -type f); do
  protoc -I=${CURRENT_DIR} -I=/usr/local/include --go-grpc_out ${CURRENT_DIR} ${x}
done