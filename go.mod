module bitbucket.org/illus10n/user_service

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.5.2
	github.com/spf13/cast v1.3.1
	github.com/stretchr/testify v1.5.1
	go.uber.org/zap v1.15.0
	google.golang.org/genproto v0.0.0-20200825200019-8632dd797987 // indirect
	google.golang.org/grpc v1.29.1
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v0.0.0-20200825214159-c4ba4cc6af4a // indirect
)
