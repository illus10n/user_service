package main

import (
	"fmt"
	pb "genproto/user_service"
	"net"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"google.golang.org/grpc"

	"bitbucket.org/illus10n/gz_user_service/storage"
	"bitbucket.org/illus10n/user_service/config"
	"bitbucket.org/illus10n/user_service/pkg/logger"
	"bitbucket.org/illus10n/user_service/service"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "user_service")
	defer logger.Cleanup(log)

	log.Info("main: pgxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	psqlString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresDatabase)

	connDb, err := sqlx.Connect("postgres", psqlString)
	if err != nil {
		log.Error("Error while connecting database: %v", logger.Error(err))
		return
	}

	storage := storage.NewStoragePg(connDb)

	customerService := service.NewCustomerService(storage, log)
	//adminService := service.NewAdminService(storage, log)

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
	s := grpc.NewServer()

	pb.RegisterCustomerServiceServer(s, customerService)
	//pb.RegisterAdminServiceServer(s, adminService)

	log.Info("main: server running",
		logger.String("port", cfg.RPCPort))

	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
