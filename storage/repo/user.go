package repo

import (
	pb "genproto/user_service"

	_ "github.com/lib/pq"
)

//UserStorageI ...
type UserStorageI interface {
	Create(user *pb.User) (*pb.User, error)
	Update(user *pb.User) (*pb.User, error)
	Delete(id string) error
	GetUser(id string) (*pb.User, error)
	GetUsers(page, limit int64) ([]*pb.User, int64, error)
	ExistsUsers(email string) (bool, error)
}
