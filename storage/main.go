package storage

import (
	"github.com/jmoiron/sqlx"

	"bitbucket.org/illus10n/user_service/storage/postgres"
	"bitbucket.org/illus10n/user_service/storage/repo"
)

//StorageI ...
type StorageI interface {
	User() repo.UserStorageI
}

type storagePg struct {
	userRepo repo.UserStorageI
}

// NewStoragePg ...
func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		userRepo: postgres.NewUserRepo(db),
	}
}

func (s storagePg) User() repo.UserStorageI {
	return s.userRepo
}
