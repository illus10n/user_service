package postgres

import (
	"database/sql"
	pb "genproto/user_service"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	"bitbucket.org/illus10n/user_service/storage/repo"
)

type userRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewUserRepo(db *sqlx.DB) repo.UserStorageI {
	return &userRepo{
		db: db,
	}
}

func (shp *userRepo) Create(user *pb.User) (*pb.User, error) {
	insertNew :=
		`INSERT INTO
		users
		(
			id,
			access_token,
			first_name,
			last_name,
			username,
			image,
			password,
			email
		)
		VALUES
		($1, $2, $3, $4, $5, $6, $7, $8)`
	_, err := shp.db.Exec(
		insertNew,
		user.GetId(),
		user.GetAccessToken(),
		user.GetFirstName(),
		user.GetLastName(),
		user.GetUsername(),
		user.GetImage(),
		user.GetPassword(),
		user.GetEmail(),
	)

	if err != nil {
		return nil, err
	}

	v, err := shp.GetUser(user.GetId())

	if err != nil {
		return nil, err
	}

	return v, nil
}

func (shp *userRepo) GetUser(id string) (*pb.User, error) {
	var (
		createdAt  time.Time
		layoutDate string = "2006-01-02 15:04:05"
		user       pb.User
		column     string
	)

	_, err := uuid.Parse(id)

	if err != nil {
		column = " email "
	} else {
		column = " id "
	}

	row := shp.db.QueryRow(`
		SELECT  id,
				access_token,
				first_name,
				last_name,
				username,
				image,
				password,
				email
				is_active,
				is_admin,
				created_at
		FROM users
		WHERE `+column+`=$1 and
		deleted_at IS NULL`, id,
	)

	err = row.Scan(
		&user.Id,
		&user.AccessToken,
		&user.FirstName,
		&user.LastName,
		&user.Username,
		&user.Image,
		&user.Password,
		&user.Email,
		&user.IsActive,
		&user.IsADmin,
		&createdAt,
	)
	if err != nil {
		return nil, err
	}

	user.CreatedAt = createdAt.Format(layoutDate)

	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (shp *userRepo) GetUsers(page, limit int64) ([]*pb.User, int64, error) {
	var (
		count      int64
		createdAt  time.Time
		layoutDate string = "2006-01-02 15:04:05"
		users      []*pb.User
	)

	offset := (page - 1) * limit

	query := `
		SELECT  id,
				access_token,
				first_name,
				last_name,
				username,
				image,
				password,
				email
				is_active,
				is_admin,
				created_at
		FROM users
		WHERE deleted_at IS NULL 
		ORDER BY created_at DESC 
		LIMIT $1 OFFSET $2`
	rows, err := shp.db.Queryx(query, limit, offset)

	if err != nil {
		return nil, 0, err
	}

	for rows.Next() {
		var user pb.User
		err = rows.Scan(
			&user.Id,
			&user.AccessToken,
			&user.FirstName,
			&user.LastName,
			&user.Username,
			&user.Image,
			&user.Password,
			&user.Email,
			&user.IsActive,
			&user.IsADmin,
			&createdAt,
		)

		if err != nil {
			return nil, 0, err
		}

		user.CreatedAt = createdAt.Format(layoutDate)
		users = append(users, &user)
	}

	row := shp.db.QueryRow(`
		SELECT count(1) 
		FROM users
		WHERE deleted_at IS NULL`,
	)

	err = row.Scan(
		&count,
	)

	return users, count, nil
}

func (shp *userRepo) Update(user *pb.User) (*pb.User, error) {
	updateQuery :=
		`UPDATE users
		 SET
			first_name=$1,
			last_name=$2,
			username=$3,
			password=$4,
			email=$5,
			image=$6,
			is_admin=$7,
			updated_at=CURRENT_TIMESTAMP
		WHERE id=$8`

	_, err := shp.db.Exec(
		updateQuery,
		user.GetFirstName(),
		user.GetLastname(),
		user.GetUsername(),
		user.GetPassword(),
		user.GetEmail,
		user.GetImage(),
		user.GetIsAdmin(),
		user.GetId(),
	)

	if err != nil {
		return nil, err
	}

	c, err := shp.GetUser(user.GetId())

	if err != nil {
		return nil, err
	}

	return c, nil
}

func (shp *userRepo) Delete(id string) error {
	result, err := shp.db.Exec(`UPDATE users SET deleted_at=CURRENT_TIMESTAMP where id=$1 and deleted_at IS NULL`, id)

	if err != nil {
		return err
	}

	if i, _ := result.RowsAffected(); i == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (shp *userRepo) ExistsUsers(email string) (bool, error) {
	var existsusers int

	row := shp.db.QueryRow(`
		SELECT count(1) 
		FROM users
		WHERE email = $1`, email,
	)

	err := row.Scan(&existsusers)

	if err != nil {
		return false, err
	}

	if existsusers == 0 {
		return false, nil
	}

	return true, nil
}

func (shp *userRepo) GetByLogin(login string) (*pb.User, error) {
	var user pb.User

	row := shp.db.QueryRow(`select id, first_name, last_name, username, password, access_token from users where username=$1 and active=true`, login)

	err := row.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Username, &user.Password, &user.AccessToken)

	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (shp *userRepo) ChangePassword(userID, password string) error {
	_, err := shp.db.Exec(`update users set password = $1 where id=$2`, password, userID)

	return err
}
