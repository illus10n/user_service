package service

import (
	"context"
	"database/sql"
	pb "genproto/user_service"

	gpb "github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	l "bitbucket.org/illus10n/user_service/pkg/logger"
	"bitbucket.org/illus10n/user_service/storage"
)User

// UserService ...
type UserService struct {
	storage storage.StorageI
	logger  l.Logger
}

// NewUserService ...
func NewUserService(strg storage.StorageI, log l.Logger) *UserService {
	return &UserService{
		storage: strg,
		logger:  log,
	}
}

// CreateUser is function for creating an user
func (s *UserService) Create(ctx context.Context, req *pb.User) (*pb.UserResponse, error) {
	user, err := s.storage.User().Create(req)
	if err != nil {
		s.logger.Error("Error while creating User in service", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.UserResponse{
		User: user,
	}, nil
}

// GetUser is function for getting an User
func (s *UserService) Get(ctx context.Context, req *pb.GetRequest) (*pb.UserResponse, error) {
	user, err := s.storage.User().GetUser(req.GetId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting an user, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting User", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}
	return &pb.UserResponse{
		User: user,
	}, nil
}

// Find is function for getting all users according to filters
func (s *UserService) Find(ctx context.Context, req *pb.FindRequest) (*pb.UsersResponse, error) {
	var users []*pb.User

	users, count, err := s.storage.User().GetUsers(req.GetPage(), req.GetLimit())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting all users, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting all Users", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.UsersResponse{
		Users: users,
		Count: count,
	}, nil
}

// UpdateUser is function for updating a User
func (s *UserService) Update(ctx context.Context, req *pb.User) (*pb.UserResponse, error) {
	user, err := s.storage.User().Update(req)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating user, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating User", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.UserResponse{
		User: user,
	}, nil
}

//DeleteUser if function for deleting User
func (s *UserService) Delete(ctx context.Context, req *pb.DeleteRequest) (*gpb.Empty, error) {
	err := s.storage.User().Delete(req.GetId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while deleting user, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while deleting User", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}
	return &gpb.Empty{}, nil
}
