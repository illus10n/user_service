CREATE TABLE IF NOT EXISTS users (
     id uuid PRIMARY KEY,
     access_token VARCHAR NOT NULL UNIQUE,
     usernname  VARCHAR(64) NOT NULL,
     firstnname  VARCHAR(64) NOT NULL,
     lastname VARCHAR(64) NOT NULL,
     password  VARCHAR NOT NULL,
     email VARCHAR(64) NOT NULL UNIQUE,
     is_active BOOLEAN NOT NULL DEFAULT TRUE,
     is_admin BOOLEAN NOT NULL DEFAULT FALSE,
     address TEXT not null,
     created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
     updated_at TIMESTAMP
);